﻿using ENCRYPTOR;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace ENCRYPTOR_TESTS
{
	[TestClass]
	public class EncrypterTests
	{
		[TestMethod]
		public void StringEncryptResultIsCorrect()
		{
			//Arrange
			string test = "Поздр2123dsdавляю!";
			string key = "скорпион";
			string expected = "Бщцфа2123dsdирщри!";

			//Act
			string result = Encryptor.Encrypt(test, key);

			//Assert
			Assert.AreEqual(expected, result);
		}


		[TestMethod]
		public void IEnumerableEncryptResultIsCorrect()
		{
			//Arrange
			List<string> listTest = new List<string> { "Поздр2123dsdавляю!", "" };
			string key = "скорпион";
			List<string> listExpected = new List<string> { "Бщцфа2123dsdирщри!", "" };

			//Act
			var listResult = Encryptor.Encrypt(listTest, key);

			//Assert
			Assert.IsTrue(listResult.SequenceEqual(listExpected));

		}


		[TestMethod]
		public void StringDecryptResultIsCorrect()
		{
			//Arrange
			string test = "Бщцфа2123dsdирщри!";
			string key = "скорпион";
			string expected = "Поздр2123dsdавляю!";

			//Act
			string result = Encryptor.Decrypt(test, key);

			//Assert
			Assert.AreEqual(expected, result);
		}


		[TestMethod]
		public void IEnumerableDecryptResultIsCorrect()
		{
			//Arrange
			List<string> listTest = new List<string> { "Бщцфа2123dsdирщри!", "" };
			string key = "скорпион";
			List<string> listExpected = new List<string> { "Поздр2123dsdавляю!", "" };

			//Act
			var listResult = Encryptor.Decrypt(listTest, key);

			//Assert
			Assert.IsTrue(listResult.SequenceEqual(listExpected));

		}
	}
}
