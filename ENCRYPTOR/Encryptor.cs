﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ENCRYPTOR
{
	public sealed class Encryptor
	{

		static List<char> _alphabet;


		public static string Encrypt(string source, string key)
		{
			//проверка наличия алфавита для построения квадрата виженера
			if (_alphabet == null)
			{
				GetAlphabet();
			}
			//проверка на null
			if (source == null || key == null)
			{
				return "";
			}

			char[] _charKey = key.ToUpper().Where(item => _alphabet.Contains(item)).ToArray();
			char[] _charSource = source.ToCharArray();
			string _encryptedString = "";
			int _position;
			int j = 0;

			for (int i = 0; i < _charSource.Length; i++)
			{
				if (_alphabet.Contains(Char.ToUpper(_charSource[i])))
				{
					_charSource[i] = Char.ToUpper(_charSource[i]);

					if (_alphabet.IndexOf(_charSource[i]) + _alphabet.IndexOf(_charKey[j]) + 1 > _alphabet.Count)
					{
						_position = _alphabet.IndexOf(_charSource[i]) + _alphabet.IndexOf(_charKey[j]) - _alphabet.Count;

						if (Char.IsUpper(source[i]))
						{
							_encryptedString += _alphabet[_position];
						}
						else
						{
							_encryptedString += Char.ToLower(_alphabet[_position]);
						}
					}
					else
					{
						_position = _alphabet.IndexOf(_charSource[i]) + _alphabet.IndexOf(_charKey[j]);
						if (Char.IsUpper(source[i]))
						{
							_encryptedString += _alphabet[_position];
						}
						else
						{
							_encryptedString += Char.ToLower(_alphabet[_position]);
						}
					}

					if (j == _charKey.Length - 1)
					{
						j = 0;
					}
					else
					{
						j++;
					}

				}
				else
				{
					_encryptedString += _charSource[i];
				}

			}

			return _encryptedString;
		}


		public static IEnumerable<string> Encrypt(IEnumerable<string> source, string key)
		{
			if (source is List<string>)
			{
				List<string> _list = new List<string>();
				foreach (var item in source)
				{
					_list.Add(Encrypt(item, key));
				}
				return _list;
			}
			if (source is HashSet<string>)
			{
				HashSet<string> _hashSet = new HashSet<string>();
				foreach (var item in source)
				{
					_hashSet.Add(Encrypt(item, key));
				}
				return _hashSet;
			}
			return source.Select(item => Encrypt(item, key)).ToList();
		}


		public static string Decrypt(string encrypted, string key)
		{
			//проверка наличия алфавита для построения квадрата виженера
			if (_alphabet == null)
			{
				GetAlphabet();
			}
			//проверка на null
			if (encrypted == null || key == null)
			{
				return "";
			}

			char[] _charKey = key.ToUpper().Where(item => _alphabet.Contains(item)).ToArray();
			char[] _charEncrypted = encrypted.ToCharArray();
			string _decryptedString = "";
			int _position;
			int j = 0;

			for (int i = 0; i < _charEncrypted.Length; i++)
			{
				if (_alphabet.Contains(Char.ToUpper(_charEncrypted[i])))
				{
					_charEncrypted[i] = Char.ToUpper(_charEncrypted[i]);

					if (_alphabet.IndexOf(_charEncrypted[i]) - _alphabet.IndexOf(_charKey[j]) < 0)
					{
						_position = _alphabet.Count - ((_alphabet.IndexOf(_charKey[j]) - _alphabet.IndexOf(_charEncrypted[i])));
						if (Char.IsUpper(encrypted[i]))
						{
							_decryptedString += _alphabet[_position];
						}
						else
						{
							_decryptedString += Char.ToLower(_alphabet[_position]);
						}
					}
					else
					{
						_position = (_alphabet.IndexOf(_charEncrypted[i]) - _alphabet.IndexOf(_charKey[j]));
						if (Char.IsUpper(encrypted[i]))
						{
							_decryptedString += _alphabet[_position];
						}
						else
						{
							_decryptedString += Char.ToLower(_alphabet[_position]);
						}
					}

					if (j == _charKey.Length - 1)
					{
						j = 0;
					}
					else
					{
						j++;
					}
				}
				else
				{
					_decryptedString += _charEncrypted[i];
				}
			}
			return _decryptedString;
		}


		public static IEnumerable<string> Decrypt(IEnumerable<string> encrypted, string key)
		{
			if (encrypted is List<string>)
			{
				List<string> _list = new List<string>();
				foreach (var item in encrypted)
				{
					_list.Add(Decrypt(item, key));
				}
				return _list;
			}
			if (encrypted is HashSet<string>)
			{
				HashSet<string> _hashSet = new HashSet<string>();
				foreach (var item in encrypted)
				{
					_hashSet.Add(Decrypt(item, key));
				}
				return _hashSet;
			}
			return encrypted.Select(item => Decrypt(item, key)).ToList();
		}


		static void GetAlphabet()
		{
			_alphabet = new List<char>();
			for (int i = 1040; i < 1072; i++)
			{
				if (_alphabet.Count == 6) _alphabet.Add('Ё');
				_alphabet.Add(Convert.ToChar(i));
			}
		}

	}
}
